import secrets

from jinja2.ext import Extension

RANDOM_STRING_CHARS = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'


class GenerateSecretExtension(Extension):
    """Jinja2 extension to create a secret key."""

    def __init__(self, environment):
        """Jinja2 Extension Constructor."""
        super().__init__(environment)

        def generate_secret(length=50, allowed_chars=RANDOM_STRING_CHARS):
            """
            Generate a secret. Based off django.utils.crypto.get_random_string.
            """
            return ''.join(
                secrets.choice(allowed_chars) for _ in range(length)
            )

        environment.globals.update(generate_secret=generate_secret)
