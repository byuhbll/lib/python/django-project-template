from django.contrib.auth.models import AbstractUser
from django.db import models
from dry.models import UserMixin


class User(AbstractUser, UserMixin):
    library_id = models.CharField(max_length=9, blank=True)
