"""
Signals.
"""
import logging

from django.contrib.auth.signals import user_logged_in

from .models import User

logger = logging.getLogger(__name__)


def update_user_info_signal(sender, request, user, **kwargs):
    """
    Function used as a Django signal receiver, called when a user logs into
    the site successfully.
    """
    pass


# Hook up the signal
user_logged_in.connect(update_user_info_signal, sender=User)
