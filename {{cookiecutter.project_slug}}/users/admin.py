from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

User = get_user_model()


class UserAdmin(UserAdmin):
    list_display = ('username', 'library_id') + UserAdmin.list_display[1:]

    fieldsets = (
        (None, {'fields': ('username', 'password', 'library_id')}),
    ) + UserAdmin.fieldsets[1:]


admin.site.register(User, UserAdmin)
