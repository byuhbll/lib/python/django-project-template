"""
Boilerplate.
"""
import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)


class {{cookiecutter.project_slug|title|replace('-','')}}Config(AppConfig):
    name = '{{cookiecutter.project_identifier}}'
    verbose_name = '{{cookiecutter.project_name|title}}'
