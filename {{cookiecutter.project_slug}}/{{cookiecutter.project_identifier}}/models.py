"""
{{ cookiecutter.project_identifier }} models.
"""
import logging

from django.db import models  # noqa: F401

logger = logging.getLogger(__name__)
