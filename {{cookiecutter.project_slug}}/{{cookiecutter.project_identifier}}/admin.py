import logging

from django.conf import settings
from django.contrib import admin

logger = logging.getLogger(__name__)

admin.site.site_header = settings.DRY.get('PROJECT_NAME', '')
admin.site.site_title = admin.site.site_header
