# -*- coding: utf-8 -*-
"""
Logging setting overrides for {{cookiecutter.project_name}}.

dry logging settings can be found here:
https://gitlab.com/byuhbll/lib/python/django-dry/-/blob/master/dry/logging.py

"""
from dry.logging import *  # noqa: F401

# App-specific settings below
# ------------------------------------------------------------------------------
