from django.urls import include, path
from dry.urls import urlpatterns as dry_urlpatterns

from {{cookiecutter.project_identifier}} import urls as {{cookiecutter.project_identifier}}_urls

project_urlpatterns = [
    # {{cookiecutter.project_identifier}} app urls
    path('', include(({{cookiecutter.project_identifier}}_urls, '{{cookiecutter.project_identifier}}')))
]

urlpatterns = project_urlpatterns + dry_urlpatterns
