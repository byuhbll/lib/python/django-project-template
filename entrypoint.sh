#!/bin/bash

# Run cookiecutter with the template (located in /dpt).
# Output to the /projects directory inside the container.
cookiecutter --output-dir /projects "$@" /dpt
