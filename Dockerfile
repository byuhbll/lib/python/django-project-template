FROM python:3.8-slim-bullseye

ARG USERNAME=dptuser
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME

# Install Git
RUN apt-get update \
    && apt-get install -y --no-install-recommends git \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install cookiecutter
RUN pip3 install cookiecutter

# Set the default git branch to main
RUN git config --global init.defaultBranch main

# Make directory for rendered projects
RUN mkdir -p /projects

USER $USERNAME
ENV SHELL /bin/bash
WORKDIR /dpt
COPY . /dpt

ENTRYPOINT ["/dpt/entrypoint.sh"]
