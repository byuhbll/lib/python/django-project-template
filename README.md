# Django Project Template

This is the project template for Django projects used by Software Engineering in the BYU Library.

## Usage

Run this command in the directory you want the project generated:

```sh
docker run --pull always -it --rm -v $(pwd):/projects \
    registry.gitlab.com/byuhbll/lib/python/django-project-template:latest
```

### Prompts
Following are the values you will be prompted for and what they are used for:

* `project_name`: The human readable project name (used in documentation and comments). (e.g. 'My Project')
* `project_identifier`: A [valid identifier](https://docs.python.org/3.8/library/stdtypes.html#str.isidentifier) to identify the project (used in directory paths
  and Python code). (e.g. 'my_project')
* `project_slug`: The kebab-case slug (e.g. 'my-project'). Used as the base path in URL. This should be the repo name.
* `description`: A short description of the project (used in the project's default README.md).
* `frontend`: Whether the frontend part of the build process (Gulp stuff for building CSS/JS/etc.) should be installed.

## Project Structure and Notes

The project structure that you end up with when using this template is to be
considered a **strong suggestion**. A lot of projects will just end up using
the template the way it is. But this structure should not be seen as a
requirement. If a different structure make more sense for the project you are
working on, feel free to change it (in consultation with the reviewer for your
project), but please document somewhere what you've changed and why you chose
to do it differently.

* `assets`: This is the images, CSS, and JavaScript. This directory contains
  the static content for the project to be built (using Gulp) and eventually
  stored in static directories.

* `project`: This directory acts as the main project directory in a standard
  Django project. It contains project-specific settings and configurations and
  shouldn't contain any application code.

  * `settings.py`: The Django settings file. This file should pull from the
	`application.yml` file/configurations to manage settings for the Django project when developing on your own machine. Should inherit from `application-base.yml` and then override or provide configuration values as needed.

  * `logging.py`: This file specifies the logging configurations for the
	project. By default, it uses the `django-dry` logging configuration.

  * `urls.py`: This is the main `urls.py` for the project. All other
	application's `urls.py` files should be included here.

* `[appname]`: Default application. This app should be thought of as the glue
  in a large project to merge functionality/interfaces/etc. from other
  applications, or as the only application in a small project. The default
  application should be thought of as a suggested application for integration
  to start and should be replaced with applications that make more sense for
  your project if need be.

* `requirements`: Instead of having a single `requirements.txt` file, this
  project has a `requirements` directory. The default requirements (used on all
  environments) are in `requirements/base.txt`. For the specific environment, a
  `requirements/[environment].txt` file should be created, referencing these
  core requirements file. Staging should use the `prod` or production
  requirements file.

* `.gitignore` : Default .gitignore file for your new project.

* `README.md`: Default README.md file for the project.

* `manage.py`: Standard Django manage file.

* `package.json`: This is essentially the `requirements.txt` for
  JavaScript/Node. It is also the configuration for front-end build process. It
  is used by npm (which is basically pip for Node).

* `application.yml` and `application-base.yml`: These YAML files contain configurations
  that are pulled into the `project/settings.py` file. The `application-base.yml`
  file should be checked in with the code and contains the base settings for
  the project. The `application.yml` file should not be checked in and is the configuration file used in development. `project/settings.py`
  should use Configuro to load `application.yml` and merge it on top of
  `application-base.yml` in memory, to allow overriding any settings the project
  needs adjusting. If you specify a DJANGO_CONFIG environement variable with a config filepath, then [Configuro](https://gitlab.com/byuhbll/lib/python/configuro) will use that one. For Kubernetes (review, staging, production) deploys we set `DJANGO_CONFIG` to point to `application-base.yml` and then override configuration values as needed with GitLab CI/CD environment variables. The configuration item to be overriden must be present in the `application-base.yml` file, so be sure to add any project specific configuration items to `application-base.yml` with default or placeholder values.


## Tips for working on this project

This project contains a `.devcontainer.json` file that will allow you to develop this project in a dev container. If you make changes to the template and then want to test things out by creating a project you can run `cookiecutter .`. If you want to have the generated project be somewhere else on your machine you can create a `.devcontainer` directory with a `devcontainer.json` file in it. Copy the contents from `.devcontainer.json` to the `.devcontainer/devcontainer.json` file and then add something like this:

```
"mounts": [
  {
    "source": "~/path/to/dir",
    "target": "/projects",
    "type": "bind"
  }
],
```

Once you build the devcontainer from your new `.devcontainer/devcontainer.json` file, run `cookiecutter -o /projects .` and this will generate the project in the `/projects` dir inside the container, but it will now also be present in the local dir you specified as the `source` in the mount configuration.
