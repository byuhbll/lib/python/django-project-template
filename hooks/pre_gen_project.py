import subprocess

PROJECT_IDENTIFIER = '{{cookiecutter.project_identifier}}'
PROJECT_SLUG = '{{cookiecutter.project_slug}}'
FRONTEND = '{{cookiecutter.frontend}}'

POP = '\033[1;31m'
C = '\033[0m'


def system(cmd):
    ret = subprocess.Popen(
        cmd,
        shell=True,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        close_fds=True,
    )
    out, err = ret.communicate()
    return (out.decode('utf-8'), err.decode('utf-8'), ret.returncode)


assert (
    PROJECT_IDENTIFIER.isidentifier()
), '{}Project identifier should be a valid identifier!{}'.format(POP, C)

assert (
    PROJECT_IDENTIFIER.islower()
), '{}Project identifier should be lowercase!{}'.format(POP, C)

assert PROJECT_SLUG.replace(
    '-', '_'
).isidentifier(), (
    '{}Project slug should only have letters, and hyphens!{}'.format(POP, C)
)

assert all(
    [w.islower() for w in PROJECT_SLUG.split('-')]
), '{}Project slug should be lowercase strings separated by hyphens (kebab-case)!{}'.format(
    POP, C
)

assert FRONTEND.lower() in [
    'n',
    'y',
], '{}frontend needs to be either "y" or "n".{}'.format(POP, C)

try:
    out, err, _ = system('python --version')
except Exception as e:
    raise AssertionError(
        '{}Could not determine Python version.{}{}'.format(POP, C, e)
    )
