"""
Post-generate hook. Sets things up and runs the project setup script.
"""
import os
import subprocess
import sys
from contextlib import contextmanager
from pathlib import Path

FRONTEND = True if '{{cookiecutter.frontend}}'.lower() == 'y' else False

verbose_value = os.environ.get('COOKIECUTTER_VERBOSE', 'FALSE')
VERBOSE = True if verbose_value == 'TRUE' else False

# Make sure we have a Halo
try:
    from halo import Halo
except ImportError:

    class Halo:
        def __init__(self, text, spinner):
            self.text = text
            self.spinner = spinner

        def start(self):
            print(self.text)

        def succeed(self, text):
            print(text)


@contextmanager
def spin(text, success):
    """
    Simple context manager to abstract out the spinner stuff.
    """
    spinner = Halo(text=text, spinner='dots')
    spinner.start()
    yield
    spinner.succeed(text=success)


def _system(command):
    """
    Helper function to run a system command. Returns a tuple containing
    whatever was piped to stdout and stderr, along with the command's return
    code.
    """
    result = subprocess.run(
        command,
        shell=True,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        encoding='utf-8',
    )

    for line in result.stdout.split('\n'):
        print(line)

    return result.stdout, result.stderr, result.returncode


def output(command):
    """
    Helper function to run a system command and print the output.
    """
    stdout, stderr, exit_code = _system(command)

    if VERBOSE or exit_code != 0:
        print(
            'command: {cmd}'.format(cmd=command),
            'stdout: {out}'.format(out=stdout.strip()),
            'stderr: {err}'.format(err=stderr.strip()),
            'exit code: {}'.format(exit_code),
            sep='\n',
            end='\n\n',
        )

        # Error
        if exit_code != 0:
            sys.exit(exit_code)


print('Finished creating project.')

PROJECT_DIR = Path(os.path.realpath(os.path.curdir))
PROJECT_IDENTIFIER = '{{cookiecutter.project_identifier}}'

if not FRONTEND:
    output('rm -r assets')
    output('rm templates/*.html')
    output('rm package.json')
    output('rm tasks/gulpfile.js')

# Create the Git repo
with spin('Initializing repo', success='Repo initialized'):
    output('git init -q')

print('Ready')
