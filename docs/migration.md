# Migration Guide

This document outlines how to upgrade an older project to use the latest
project template.


## Table of Contents

- [General Process](#general)
- [Recent Changes to the Templates](#recent)
- [Frontend Migration](#frontend)


<h2 id="general">General Process</h2>

1. Create and switch to new branch.
2. Rename your current project folder to `[project]-old`.
3. Run cookiecutter in the directory where `[project]-old` is located to generate the new project skeleton.
4. Delete the `.git/` directory in the newly generated project.
5. Copy the `.git/` directory from your old project into the newly created project.
6. Resolve issues (run `git status` to find out what those issues are):
   - _Deleted files:_ Determine if that file needs to be pulled back into the project (`git checkout [file]`) or actually should be deleted or removed (`git add [file]`).
   - _Untracked (added) files:_ Determine if you need the file. Add the file if you do (`git add [file]`) or remove the file if you don’t (`rm [file]`).
   - _Modified or moved files:_ You’ll need to compare the files to the old version and update the new file to look the way it should. This will probably take the bulk of the time it takes to update a project. After the file looks the way it should add the changes (_git add [file]_).
7. Remove the [project]-old folder.
8. Push branch to GitLab e.g. `git push -u origin [branch name]`.

Notes:
- Any custom apps (i.e. Django apps) that the project template has no idea about will appear deleted. These should be able to be added back right away with a simple `git checkout [app name]`.

<h2 id="general">Helps</h2>

### Project name, slug, and identifier

- Project name: A normal title e.g. `Instruction Scheduler`
- Project slug: Kebab-case project name, used as the base path in URL e.g. `instruction-scheduler`
- Project indentifier: Valid Python identifier, e.g. `instruction_scheduler`
