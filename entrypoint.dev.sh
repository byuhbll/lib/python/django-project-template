#!/bin/bash

# Update pip
python -m pip install -U pip

# Install dev dependencies
python -m pip install -r /dpt/dev_requirements.txt

# Install pre-commit
pre-commit install
