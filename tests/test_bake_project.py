import contextlib

from cookiecutter.utils import rmtree


@contextlib.contextmanager
def bake_in_temp_dir(cookies, *args, **kwargs):
    result = cookies.bake(*args, **kwargs)
    try:
        yield result
    finally:
        rmtree(str(result.project_path))


def _test_bake(cookies, check_ci=False):
    with bake_in_temp_dir(cookies, extra_context={'frontend': 'n'}) as result:
        assert result.project_path.is_dir()


def test_bake_and_run_tests(cookies):
    _test_bake(cookies, check_ci=False)


def test_ci_bake_and_run_tests(cookies):
    _test_bake(cookies, check_ci=True)
